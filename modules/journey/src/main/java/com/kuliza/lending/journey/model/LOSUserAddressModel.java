package com.kuliza.lending.journey.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "los_user_address")
public class LOSUserAddressModel extends BaseModel {

	@Column(nullable = true)
	private String addressType;

	@Column(nullable = true)
	private String addressLine1;

	@Column(nullable = true)
	private String addressLine2;

	@Column(nullable = true)
	private String cityDistrict;

	@Column(nullable = true)
	private String state;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userId", nullable = false)
	private LOSUserModel losUserModel;

	public LOSUserAddressModel() {
		super();
		this.setIsDeleted(false);
	}

	public LOSUserAddressModel(String addressType, String addressLine1, String addressLine2, String cityDistrict,
			String state, LOSUserModel losUserModel) {
		super();
		this.setIsDeleted(false);
		this.addressType = addressType;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.cityDistrict = cityDistrict;
		this.state = state;
		this.losUserModel = losUserModel;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCityDistrict() {
		return cityDistrict;
	}

	public void setCityDistrict(String cityDistrict) {
		this.cityDistrict = cityDistrict;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public LOSUserModel getLosUserModel() {
		return losUserModel;
	}

	public void setLosUserModel(LOSUserModel losUserModel) {
		this.losUserModel = losUserModel;
	}

}
