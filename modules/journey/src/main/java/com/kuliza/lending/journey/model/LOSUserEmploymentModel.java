package com.kuliza.lending.journey.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "los_user_employment")

public class LOSUserEmploymentModel extends BaseModel {

	@Column(nullable = true)
	private String employmentStatus;

	@Column(nullable = true)
	private String companyName;

	@Column(nullable = true)
	private String companyType;

	@Column(nullable = true)
	private String occupation;

	@Column(nullable = true)
	private String position;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userId", nullable = false)
	private LOSUserModel losUserModel;

	public LOSUserEmploymentModel() {
		super();
		this.setIsDeleted(false);
	}

	public LOSUserEmploymentModel(String employmentStatus, String companyName, String companyType, String occupation,
			String position, LOSUserModel losUserModel) {
		super();
		this.setIsDeleted(false);
		this.employmentStatus = employmentStatus;
		this.companyName = companyName;
		this.companyType = companyType;
		this.occupation = occupation;
		this.position = position;
		this.losUserModel = losUserModel;
	}

	public String getEmploymentStatus() {
		return employmentStatus;
	}

	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public LOSUserModel getLosUserModel() {
		return losUserModel;
	}

	public void setLosUserModel(LOSUserModel losUserModel) {
		this.losUserModel = losUserModel;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

}
