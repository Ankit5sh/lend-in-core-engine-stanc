package com.kuliza.lending.journey.customer_dashboard.pojo;

import java.util.List;
import java.util.Map;

import com.kuliza.lending.journey.model.LOSUserAddressModel;
import com.kuliza.lending.journey.model.LOSUserEmploymentModel;
import com.kuliza.lending.journey.model.LOSUserModel;


public class CustomerDashboardProfile {

	private CustomerDashboardEmploymentInfo employmentInfo;
	private CustomerDashboardAddressInfo addressInfo;
	private CustomerDashboardCustomerInfo customerInfo;

	public CustomerDashboardProfile() {
		super();
		this.employmentInfo = new CustomerDashboardEmploymentInfo();
		this.addressInfo = new CustomerDashboardAddressInfo();
		this.customerInfo = new CustomerDashboardCustomerInfo();
	}

	public CustomerDashboardProfile(LOSUserModel customerInfo,
			List<LOSUserAddressModel> addressInfoList, LOSUserEmploymentModel employmentInfo) {
		super();
		this.employmentInfo = new CustomerDashboardEmploymentInfo(employmentInfo);
		this.addressInfo = new CustomerDashboardAddressInfo(addressInfoList);
		this.customerInfo = new CustomerDashboardCustomerInfo(customerInfo);
	}
	
	public CustomerDashboardProfile(CustomerDashboardEmploymentInfo employmentInfo,
			CustomerDashboardAddressInfo addressInfo, CustomerDashboardCustomerInfo customerInfo) {
		super();
		this.employmentInfo = employmentInfo;
		this.addressInfo = addressInfo;
		this.customerInfo = customerInfo;
	}

	public CustomerDashboardProfile(Map<String, Object> data) {
		super();
		this.employmentInfo = new CustomerDashboardEmploymentInfo(data);
		this.addressInfo = new CustomerDashboardAddressInfo(data);
		this.customerInfo = new CustomerDashboardCustomerInfo(data);
	}

	public CustomerDashboardEmploymentInfo getEmploymentInfo() {
		return employmentInfo;
	}

	public void setEmploymentInfo(CustomerDashboardEmploymentInfo employmentInfo) {
		this.employmentInfo = employmentInfo;
	}

	public CustomerDashboardAddressInfo getAddressInfo() {
		return addressInfo;
	}

	public void setAddressInfo(CustomerDashboardAddressInfo addressInfo) {
		this.addressInfo = addressInfo;
	}

	public CustomerDashboardCustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerDashboardCustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

}
