package com.kuliza.lending.utils;

import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.UserSessionRepresentation;

/***
 * 
 * @author Naveen Dhalaria
 *
 */

public class AuthUtils {

	/**
	 * Detects if the current device is an iPhone.
	 */
	public static boolean detectIphone(String userAgent) {
		// The iPod touch says it's an iPhone! So let's disambiguate.
		return userAgent.indexOf(AuthConstants.DEVICE_TYPE_IPHONE) != -1 && !detectIpod(userAgent);
	}

	/**
	 * Detects if the current device is an iPod Touch.
	 */
	public static boolean detectIpod(String userAgent) {
		return userAgent.indexOf(AuthConstants.DEVICE_TYPE_IPHONE) != -1;
	}

	/**
	 * Detects if the current device is an iPhone or iPod Touch.
	 */
	public static boolean detectIphoneOrIpod(String userAgent) {
		// We repeat the searches here because some iPods may report themselves as an
		// iPhone, which would be okay.
		return userAgent.indexOf(AuthConstants.DEVICE_TYPE_IPHONE) != -1
				|| userAgent.indexOf(AuthConstants.DEVICE_TYPE_IPOD) != -1;
	}

	/**
	 * Detects if the current device is an Android OS-based device.
	 */
	public static boolean detectAndroid(String userAgent) {
		return userAgent.indexOf(AuthConstants.DEVICE_TYPE_ANDRIOD) != -1;
	}
	
	
	/**
	 * get the ClientID from client Name
	 * @param clientName
	 * @return
	 */
	public static String getClientID(String clientName, RealmResource resource) {
		String clientId = resource.clients().findAll().stream().filter(cr -> cr.getClientId().equals(clientName))
				.findFirst().get().getId();
		return clientId;

	}
	
	/***
	 * get offline Sessions for user in Client
	 * @param clientName
	 * @param userId
	 * @param resource
	 * @return
	 */
	public static List<UserSessionRepresentation> getOfflineSessionForUserInClient(String clientName, String userId,
			RealmResource resource) {
		List<UserSessionRepresentation> offlineSession = resource.users().get(userId)
				.getOfflineSessions(getClientID(clientName, resource));
		return offlineSession;

	}
	
	public static boolean isAdminUser(String accessToken) {
		boolean isAdmin = false;
		String[] split_string = accessToken.split("\\.");
		String base64EncodedBody = split_string[1];
		Base64 base64 = new Base64(true);
		String body = new String(base64.decode(base64EncodedBody));
		JSONObject realm_resource = (JSONObject) (new JSONObject(body)).get(AuthConstants.REALM_ACCESS_RESOURCE);
		if (realm_resource != null && realm_resource.has(AuthConstants.REALM_MANAGEMENT_RESOURCE)) {
			if (realm_resource.getJSONObject(AuthConstants.REALM_MANAGEMENT_RESOURCE).has(AuthConstants.ROLES_RESOURCE)) {
				int count = realm_resource.getJSONObject(AuthConstants.REALM_MANAGEMENT_RESOURCE)
						.getJSONArray(AuthConstants.ROLES_RESOURCE).length();
				isAdmin = true;
			}
		}
		return isAdmin;
	}
	

}
