package com.kuliza.lending.configurator.models;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface VariableDao extends CrudRepository<Variable, Long> {
	public Variable findById(long id);

	public Variable findByIdAndIsDeleted(long id, boolean isDeleted);

	public Variable findByNameAndProductId(String name, long productId);

	public Variable findByNameAndProductIdAndIsDeleted(String name, long productId, boolean isDeleted);

	public List<Variable> findByProductId(long productId);

	public List<Variable> findByProductIdAndIsDeleted(long productId, boolean isDeleted);

	public Variable findByIdAndProductIdAndIsDeleted(long id, long productId, boolean isDeleted);

	public List<Variable> findByProductIdAndCategoryAndIsDeleted(long productId, String category, boolean isDeleted);
}
