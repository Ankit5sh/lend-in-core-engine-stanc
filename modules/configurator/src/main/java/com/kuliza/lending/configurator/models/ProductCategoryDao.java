package com.kuliza.lending.configurator.models;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ProductCategoryDao extends CrudRepository<ProductCategory, Long> {

	public ProductCategory findById(long id);

	public ProductCategory findByIdAndIsDeleted(long id, boolean isDeleted);

	public ProductCategory findByNameAndIsDeleted(String name, boolean isDeleted);

	public List<ProductCategory> findByIsDeleted(boolean isDeleted);

	public List<ProductCategory> findAll();

}
