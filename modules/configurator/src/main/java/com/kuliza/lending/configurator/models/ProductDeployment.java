package com.kuliza.lending.configurator.models;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "ce_product_deployment")
@Where(clause = "is_deleted=0")
public class ProductDeployment extends BaseModel {

	@Column(columnDefinition = "tinyint(1) default 0", nullable = false)
	private boolean status;
	@Column(nullable = false)
	private String identifier;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "productId", nullable = false)
	private Product product;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userId", nullable = false)
	private User user;

	public ProductDeployment() {
		super();
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
	}

	public ProductDeployment(User user, Product product, String identifier) {
		this.user = user;
		this.product = product;
		this.identifier = identifier;
		this.status = true;
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
	}

	public ProductDeployment(long id, boolean status, String identifier, Product product, User user) {
		super();
		this.setId(id);
		this.status = status;
		this.identifier = identifier;
		this.product = product;
		this.user = user;
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
