package com.kuliza.lending.configurator.validators;

import com.kuliza.lending.configurator.models.*;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewProduct;
import com.kuliza.lending.configurator.pojo.UpdateProduct;
import com.kuliza.lending.configurator.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ProductDataValidator extends BasicDataValidator {

	@Autowired
	private ProductDao productDao;

	@Autowired
	private VariableDao variableDao;

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private RuleDao ruleDao;

	@Autowired
	private ExpressionDao expressionDao;

	@Autowired
	private ProductCategoryDao productCategoryDao;

	public GenericAPIResponse validateGetProductList(String productCategoryId, String status) throws Exception {
		GenericAPIResponse response = null;
		if (productCategoryId != null && !productCategoryId.matches("[0-9]+")) {
			response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
					Constants.INVALID_PRODUCT_CATEGORY_ID_MESSAGE);
		}

		if (!status.matches("[0-9]+")) {
			response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
					Constants.INVALID_STATUS_MESSAGE);
		}
		return response;
	}

	public void validateNewProduct(SubmitNewProduct obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		if (productDao.findByNameAndUserIdAndIsDeleted(obj.getProductName(), Long.parseLong(obj.getUserId()),
				false) != null) {
			errors.rejectValue(Constants.PRODUCT_NAME, Constants.INVALID_PRODUCT_NAME,
					Constants.PRODUCT_NAME_TAKEN_MESSAGE);
		}
		if (productCategoryDao.findByIdAndIsDeleted(Long.parseLong(obj.getProductCategoryId()), false) == null) {
			errors.rejectValue(Constants.PRODUCT_CATEGORY_ID, Constants.INVALID_PRODUCT_CATEGORY_ID,
					Constants.INVALID_PRODUCT_CATEGORY_MESSAGE);
		}
		if (!obj.getTemplateProductId().equals("")) {
			Product templateProduct = productDao.findByIdAndUserIdAndIsDeleted(
					Long.parseLong(obj.getTemplateProductId()), Long.parseLong(obj.getUserId()), false);
			if (templateProduct == null) {
				errors.rejectValue(Constants.TEMPLATE_PRODUCT_ID, Constants.INVALID_TEMPLATE_PRODUCT_ID,
						Constants.INVALID_TEMPLATE_PRODUCT_ID_MESSAGE);
			} else if (templateProduct.getProductCategory().getId() != Long.parseLong(obj.getProductCategoryId())) {
				errors.rejectValue(Constants.PRODUCT_CATEGORY_ID, Constants.INVALID_PRODUCT_CATEGORY_ID,
						Constants.PRODUCT_CATEGORY_MISMATCH_MESSAGE);
			}
		}

	}

	public GenericAPIResponse validateCloneProduct(String productId, String userId) throws Exception {
		GenericAPIResponse response = null;
		if (!validateProductId(productId, userId)) {
			response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
					Constants.INVALID_PRODUCT_ID_MESSAGE);
		} else if (productDao.findByIdAndIsDeleted(Long.parseLong(productId), false).getStatus() < 2) {
			response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
					Constants.PRODUCT_CANNOT_BE_CLONED_MESSAGE);
		}
		return response;
	}

	public void validateUpdateProduct(UpdateProduct obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		if (!validateProductId(obj.getProductId(), obj.getUserId())) {
			errors.rejectValue(Constants.PRODUCT_ID, Constants.INVALID_PRODUCT_ID,
					Constants.INVALID_PRODUCT_ID_MESSAGE);
		} else {
			Product product = productDao.findByIdAndIsDeleted(Long.parseLong(obj.getProductId()), false);
			if (product.getStatus() > 0) {
				errors.rejectValue(Constants.PRODUCT_ID, Constants.INVALID_PRODUCT_ID,
						Constants.PRODUCT_NOT_EDITABLE_MESSAGE);
			} else {
				product = productDao.findByNameAndUserIdAndIsDeleted(obj.getProductName(),
						Long.parseLong(obj.getUserId()), false);
				if (product != null) {
					errors.rejectValue(Constants.PRODUCT_NAME, Constants.INVALID_PRODUCT_NAME,
							Constants.PRODUCT_NAME_TAKEN_MESSAGE);
				}
			}
		}
	}

	public GenericAPIResponse validatePublishProduct(String productId, String userId) throws Exception {
		GenericAPIResponse response = null;
		Map<String, Object> data = new HashMap<>();
		List<Map<String, Object>> errors = new ArrayList<>();
		if (!validateProductId(productId, userId)) {
			response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
					Constants.INVALID_PRODUCT_ID_MESSAGE);
		} else {
			if (productDao.findByIdAndIsDeleted(Long.parseLong(productId), false).getStatus() > 1) {
				response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
						Constants.PRODUCT_ALREADY_DEPLOYED_MESSAGE);
			} else {
				if (variableDao.findByProductIdAndIsDeleted(Long.parseLong(productId), false).isEmpty()) {
					Map<String, Object> error = new HashMap<>();
					error.put(Constants.DATA_MESSAGE_KEY, Constants.VARIABLES_MISSING_MESSAGE);
					errors.add(error);
					data.put(Constants.DATA_ERRORS_KEY, errors);
					response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE, data);
				} else {
					List<Group> allGroups = groupDao.findByProductIdAndIsDeleted(Long.parseLong(productId), false);
					if (allGroups.isEmpty()) {
						Map<String, Object> error = new HashMap<>();
						error.put(Constants.DATA_MESSAGE_KEY, Constants.GROUP_MISSING_MESSAGE);
						errors.add(error);
						data.put(Constants.DATA_ERRORS_KEY, errors);
						response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
								data);
					} else {
						for (Group group : allGroups) {
							if (ruleDao.findByGroupIdAndIsDeleted(group.getId(), false).isEmpty()) {
								Map<String, Object> error = new HashMap<>();
								error.put(Constants.DATA_MESSAGE_KEY,
										"Rules Missing For Group : ".concat(group.getName()));
								errors.add(error);
							}

							if (expressionDao.findByGroupIdAndIsDeleted(group.getId(), false).isEmpty()) {
								Map<String, Object> error = new HashMap<>();
								error.put(Constants.DATA_MESSAGE_KEY,
										"Expression Missing For Group : ".concat(group.getName()));
								errors.add(error);
							}
						}

						if (!errors.isEmpty()) {
							data.put(Constants.DATA_ERRORS_KEY, errors);
							response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
									data);
						}

						if (response == null && expressionDao
								.findByProductIdAndIsDeleted(Long.parseLong(productId), false).isEmpty()) {
							Map<String, Object> error = new HashMap<>();
							error.put(Constants.DATA_MESSAGE_KEY, Constants.PRODUCT_EXPRESSION_MISSING_MESSAGE);
							errors.add(error);
							data.put(Constants.DATA_ERRORS_KEY, errors);
							response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
									data);
						}
					}
				}
			}
		}
		return response;
	}

	public GenericAPIResponse validateEditProduct(String productId, String userId) throws Exception {
		GenericAPIResponse response = null;
		if (!validateProductId(productId, userId)) {
			response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
					Constants.INVALID_PRODUCT_ID_MESSAGE);
		} else {
			Product product = productDao.findByIdAndIsDeleted(Long.parseLong(productId), false);
			if (product.getStatus() > 1) {
				response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
						Constants.PRODUCT_NOT_EDITABLE_MESSAGE);
			} else if (product.getStatus() == 0) {
				response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
						Constants.PRODUCT_ALREADY_EDITABLE_MESSAGE);
			}
		}
		return response;
	}

	public GenericAPIResponse validateDeployProduct(String productId, String userId) throws Exception {
		GenericAPIResponse response = null;
		if (!validateProductId(productId, userId)) {
			response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
					Constants.INVALID_PRODUCT_ID_MESSAGE);
		} else {
			Product product = productDao.findByIdAndIsDeleted(Long.parseLong(productId), false);
			if (product.getStatus() == 0) {
				response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
						Constants.PRODUCT_NOT_PUBLISHED_MESSAGE);
			} else if (product.getStatus() == 2) {
				response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
						Constants.PRODUCT_CURRENTLY_DEPLOYED_MESSAGE);
			}
		}
		return response;
	}

}
