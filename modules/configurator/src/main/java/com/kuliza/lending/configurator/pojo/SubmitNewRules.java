package com.kuliza.lending.configurator.pojo;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class SubmitNewRules {

	@NotNull(message = "newRulesList is a required key")
	@NotEmpty(message = "newRulesList cannot be empty")
	@Valid
	private List<NewRule> newRulesList;

	private String userId;
	private String productId;
	private String groupId;

	public SubmitNewRules() {
		this.newRulesList = new ArrayList<>();
	}

	public SubmitNewRules(List<NewRule> newRulesList) {
		this.newRulesList = newRulesList;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public List<NewRule> getNewRulesList() {
		return newRulesList;
	}

	public void setNewRulesList(List<NewRule> newRulesList) {
		this.newRulesList = newRulesList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("newRulesList : [ ");
	// for (int i = 0; i < newRulesList.size(); i++) {
	// inputData.append(newRulesList.get(i).toString());
	// if (i < newRulesList.size() - 1) {
	// inputData.append(", ");
	// }
	// }
	// inputData.append("] }");
	// return inputData.toString();
	// }

}
