package com.kuliza.lending.configurator.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kuliza.lending.configurator.serializers.VariableExpressionSerializer;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "ce_variable", uniqueConstraints = { @UniqueConstraint(columnNames = { "name", "productId" }) })
@Where(clause = "is_deleted=0")
public class Variable extends BaseModelWithName {

	@Column(nullable = false)
	private String type;
	@Column(nullable = false)
	private String source;
	@Column(nullable = false)
	private String category;
	@Column(nullable = false)
	private String description;
	@JsonSerialize(converter = VariableExpressionSerializer.class)
	@Column(length = 10000, nullable = false)
	private String expression;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "productId", nullable = false)
	private Product product;

	public Variable() {
		super();
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
	}

	public Variable(String name, String type, String source, String category, String description, String expression,
			Product product) {
		this.setName(name);
		this.type = type;
		this.source = source;
		this.category = category;
		this.description = description;
		this.expression = expression;
		this.product = product;
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
	}

	public Variable(String name, String type, String source, String category, String description, Product product) {
		this.setName(name);
		this.type = type;
		this.source = source;
		this.category = category;
		this.description = description;
		this.product = product;
		this.expression = null;
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
	}

	public Variable(long id, String name, String type, String source, String category, String description,
			String expression, Product product) {
		super();
		this.setId(id);
		this.setName(name);
		this.type = type;
		this.source = source;
		this.category = category;
		this.description = description;
		this.expression = expression;
		this.product = product;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonIgnore
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

}
