package com.kuliza.lending.configurator.validators;

import com.kuliza.lending.configurator.models.*;
import com.kuliza.lending.configurator.utils.Constants;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import java.util.*;

@Component
public class BasicDataValidator {

	@Autowired
	private RuleDao ruleDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private VariableDao variableDao;

	@Autowired
	private ExpressionDao expressionDao;

	public boolean validateProductId(String productId, String userId) throws Exception {
		return productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(productId), Long.parseLong(userId),
				false) == null ? false : true;
	}

	public boolean validateGroupId(String groupId, String productId) throws Exception {
		return groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(groupId), Long.parseLong(productId),
				false) == null ? false : true;
	}

	public boolean validateVariableId(String variableId, String productId) throws Exception {
		return variableDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(variableId), Long.parseLong(productId),
				false) == null ? false : true;
	}

	public boolean validateRuleId(String ruleId, String groupId) throws Exception {
		return ruleDao.findByIdAndGroupIdAndIsDeleted(Long.parseLong(ruleId), Long.parseLong(groupId), false) == null
				? false : true;
	}

	public boolean validateExpressionId(String expressionId, String groupId, String productId) throws Exception {
		if (!groupId.equals("")) {
			if (expressionDao.findByIdAndGroupIdAndIsDeleted(Long.parseLong(expressionId), Long.parseLong(groupId),
					false) == null) {
				return false;
			}
		} else {
			if (expressionDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(expressionId), Long.parseLong(productId),
					false) == null) {
				return false;
			}
		}
		return true;
	}

	// variableId parameter is added to catch such cases when person is trying
	// to update variable expression
	// and uses same variable's name in expression for which it is updating the
	// expression.
	// Example : variable name is : "age" and expression he is trying to submit
	// is : "age + cibil"
	// So it is wrong because use the variable in its own expression is not
	// permitted
	@SuppressFBWarnings("REC_CATCH_EXCEPTION")
	public boolean validateExpressionString(String expressionString, boolean flag, String productId, Errors errors)
			throws Exception {
		ArrayList<String> expressionList = new ArrayList<>(
				Arrays.asList(expressionString.split(Constants.EXPRESSION_TOKENIZER_REGEX)));
		int cellNo = 1;
		// Dummy expression is an expression with just variable names
		// changed to dummy excel cell to validate expression in Excel
		// processor
		StringBuilder dummyExpression = new StringBuilder();
		for (String item : expressionList) {
			if (item.matches(Constants.NAME_REGEX) && !item.matches(Constants.NOT_VARIABLE_NAMES_REGEX)) {
				if (flag) {
					Variable variable = variableDao.findByNameAndProductIdAndIsDeleted(item, Long.parseLong(productId),
							false);
					if (variable == null) {
						errors.rejectValue(Constants.EXPRESSION_STRING, Constants.INVALID_EXPRESSION_STRING,
								"Expression String contains invalid variable " + item);
						return false;
					}
					dummyExpression.append("A" + cellNo);
					cellNo++;
				} else {
					Group group = groupDao.findByNameAndProductIdAndIsDeleted(item, Long.parseLong(productId), false);
					if (group == null) {
						errors.rejectValue(Constants.EXPRESSION_STRING, Constants.INVALID_EXPRESSION_STRING,
								"Expression String contains invalid variable " + item);
						return false;
					}
					// Dummy excel cells location used just to verify
					// expression in excel
					dummyExpression.append("A" + cellNo);
					cellNo++;
				}
			} else {
				dummyExpression.append(item);
			}
		}
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet spreadsheet = workbook.createSheet("dummy_expression_Sheet");
		XSSFRow newrow = spreadsheet.createRow(2);
		XSSFCell cell = newrow.createCell(2);
		cell.setCellType(CellType.FORMULA);
		try {
			// Here exception is not thrown because through exception only
			// we come to know validity of expression.
			FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
			cell.setCellFormula(dummyExpression.toString());
			evaluator.evaluateInCell(cell);
		} catch (Exception e) {
			workbook.close();
			return false;
		}
		workbook.close();
		return true;
	}

	public void validateProductGroup(String userId, String productId, String groupId, Errors errors) throws Exception {
		if (!validateProductId(productId, userId)) {
			errors.rejectValue(Constants.PRODUCT_ID, Constants.INVALID_PRODUCT_ID,
					Constants.INVALID_PRODUCT_ID_MESSAGE);
		} else {
			if (productDao.findByIdAndIsDeleted(Long.parseLong(productId), false).getStatus() > 0) {
				errors.rejectValue(Constants.PRODUCT_ID, Constants.INVALID_PRODUCT_ID,
						Constants.PRODUCT_NOT_EDITABLE_MESSAGE);
			} else {
				if (!groupId.equals("") && !validateGroupId(groupId, productId)) {
					errors.rejectValue(Constants.GROUP_ID, Constants.INVALID_GROUP_ID,
							Constants.INVALID_GROUP_ID_MESSAGE);
				}
			}
		}
	}

	public void validateProductVariable(String userId, String productId, Errors errors) throws Exception {
		if (!validateProductId(productId, userId)) {
			errors.rejectValue(Constants.PRODUCT_ID, Constants.INVALID_PRODUCT_ID,
					Constants.INVALID_PRODUCT_ID_MESSAGE);
		} else {
			if (productDao.findByIdAndIsDeleted(Long.parseLong(productId), false).getStatus() > 0) {
				errors.rejectValue(Constants.PRODUCT_ID, Constants.INVALID_PRODUCT_ID,
						Constants.PRODUCT_NOT_EDITABLE_MESSAGE);
			}
		}
	}

	// This a validator cum utilility function. It checks for cyclicity and
	// gives list of all required variables to calculate a particular variable
	// or Group Expression
	public boolean findAllRequiredVariablesForDerivedVariableOrExpression(String processingVariableName,
			String expressionString, Set<Variable> allRequiredVariables, Set<Variable> allProcessedVariables,
			Set<Variable> primitiveVariables, Set<Variable> derivedVariable) {
		ArrayList<String> expressionList = new ArrayList<>(
				Arrays.asList(expressionString.split(Constants.EXPRESSION_TOKENIZER_REGEX)));
		for (String item : expressionList) {
			item = item.trim();
			if (item.matches("^(ID_)[0-9]+$")) {
				int indexOfUnderScore = item.indexOf('_');
				long variableId = Long.parseLong(item.substring(indexOfUnderScore + 1));
				Variable variable = variableDao.findByIdAndIsDeleted(variableId, false);
				if (variable.getName().equals(processingVariableName)) {
					return false;
				} else {
					if (variable.getCategory().equals(Constants.PRIMITIVE)) {
						primitiveVariables.add(variable);
						allRequiredVariables.add(variable);
					} else {
						derivedVariable.add(variable);
						if (!allProcessedVariables.contains(variable)) {
							allProcessedVariables.add(variable);
							findAllRequiredVariablesForDerivedVariableOrExpression(processingVariableName,
									variable.getExpression(), allRequiredVariables, allProcessedVariables,
									primitiveVariables, derivedVariable);
						}
					}
				}
			}
		}
		return true;
	}

	// This a validator cum utilility function gives list of all required
	// variables for a Group
	public Set<Map<String, String>> findAllVariablesRequiredForGroup(Long groupId, Set<Variable> allProcessedVariables,
			Set<Variable> primitiveVariables, Set<Variable> derivedVariable) {
		Set<Map<String, String>> allRequiredVariablesForGroup = new HashSet<>();
		List<Rule> rules = ruleDao.findByGroupIdAndIsDeletedAndIsActive(groupId, false, true);
		for (Rule rule : rules) {
			if (rule.getVariable().getCategory().equals(Constants.PRIMITIVE)) {
				Map<String, String> variable = new HashMap<>();
				variable.put("name", rule.getVariable().getName());
				variable.put("type", rule.getVariable().getType());
				variable.put("description", rule.getVariable().getDescription());
				allRequiredVariablesForGroup.add(variable);
				primitiveVariables.add(rule.getVariable());
			} else if (!allProcessedVariables.contains(rule.getVariable())) {
				derivedVariable.add(rule.getVariable());
				allProcessedVariables.add(rule.getVariable());
				Variable ruleVariable = rule.getVariable();
				Set<Variable> allRequiredVariablesForThisVariable = new HashSet<>();
				findAllRequiredVariablesForDerivedVariableOrExpression(ruleVariable.getName(),
						ruleVariable.getExpression(), allRequiredVariablesForThisVariable, allProcessedVariables,
						primitiveVariables, derivedVariable);
				for (Variable v : allRequiredVariablesForThisVariable) {
					Map<String, String> variable = new HashMap<>();
					variable.put("name", v.getName());
					variable.put("type", v.getType());
					variable.put("description", v.getDescription());
					allRequiredVariablesForGroup.add(variable);
				}
			}
		}
		List<Expression> allGroupExpressions = expressionDao.findByGroupIdAndIsDeleted(groupId, false);
		for (Expression expression : allGroupExpressions) {
			Set<Variable> allRequiredVariablesForThisExpression = new HashSet<>();
			findAllRequiredVariablesForDerivedVariableOrExpression("", expression.getExpressionString(),
					allRequiredVariablesForThisExpression, allProcessedVariables, primitiveVariables, derivedVariable);
			for (Variable v : allRequiredVariablesForThisExpression) {
				Map<String, String> variable = new HashMap<>();
				variable.put("name", v.getName());
				variable.put("type", v.getType());
				variable.put("description", v.getDescription());
				allRequiredVariablesForGroup.add(variable);
			}
		}
		return allRequiredVariablesForGroup;
	}

	// This a validator cum utilility function gives list of all required
	// variables for a product
	public Set<Map<String, String>> findAllVariablesRequiredForProduct(Long productId, Set<Variable> primitiveVariables,
			Set<Variable> derivedVariable) {
		Set<Map<String, String>> allRequiredVariablesForProduct = new HashSet<>();
		Set<Variable> allProcessedVariables = new HashSet<>();
		Product product = productDao.findByIdAndIsDeleted(productId, false);
		List<Group> allGroups = groupDao.findByProductIdAndIsDeleted(product.getId(), false);
		for (Group group : allGroups) {
			allRequiredVariablesForProduct.addAll(findAllVariablesRequiredForGroup(group.getId(), allProcessedVariables,
					primitiveVariables, derivedVariable));
		}
		return allRequiredVariablesForProduct;
	}

}
