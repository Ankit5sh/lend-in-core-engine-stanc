package com.kuliza.lending.collections.controllers;


import javax.validation.Valid;

import com.kuliza.lending.collections.utils.Constants;
import com.kuliza.lending.collections.model.DecisionTableModel;
import com.kuliza.lending.collections.pojo.DecisionTableModelRepresentation;
import com.kuliza.lending.collections.pojo.DecisionTableRequestRepresentation;
import com.kuliza.lending.collections.services.DecisionTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(Constants.DECISION_TABLE_URL)
public class DecisionTableController {


    @Autowired
    private DecisionTableService decisionTableService;

    @PostMapping(Constants.CREATE_DECISION_TABLE_URL)
    public ResponseEntity<DecisionTableModel> createAndDeployDecisionTable(@Valid  @RequestBody DecisionTableModelRepresentation decisionTableModelRepresentation) throws Exception{
        return new ResponseEntity<DecisionTableModel>(decisionTableService.addDecisionTable(decisionTableModelRepresentation), HttpStatus.CREATED);
    }

    @PostMapping(Constants.DEPLOY_DECISION_TABLE_URL)
    public ResponseEntity<DecisionTableModel> deployDecisionTable(@Valid @RequestBody DecisionTableRequestRepresentation decisionTableRequestRepresentation) throws Exception{
        return new ResponseEntity<DecisionTableModel>(decisionTableService.deployDecisionTable(decisionTableRequestRepresentation.getDecisionKey(),
                                                                                               decisionTableRequestRepresentation.getVersion()), HttpStatus.OK);
    }

    @GetMapping(Constants.FETCH_DECISION_TABLE_URL)
    public ResponseEntity<DecisionTableModelRepresentation> fetchDecisionTable(@Valid @RequestParam String decisionKey,
                                                                 @RequestParam(required=false) Integer version) throws Exception{
        return new ResponseEntity<DecisionTableModelRepresentation>(decisionTableService.fetchDecisionTableDefinition(decisionKey, version), HttpStatus.OK);
    }

    @DeleteMapping(Constants.DELETE_DECISION_TABLE_URL)
    public ResponseEntity<DecisionTableModel> deleteDecisionTable(@Valid @RequestBody DecisionTableRequestRepresentation decisionTableRequestRepresentation) throws Exception{
        return new ResponseEntity<DecisionTableModel>(decisionTableService.deleteDecisionTable(decisionTableRequestRepresentation.getDecisionKey(),
                decisionTableRequestRepresentation.getVersion()), HttpStatus.OK);
    }


}
