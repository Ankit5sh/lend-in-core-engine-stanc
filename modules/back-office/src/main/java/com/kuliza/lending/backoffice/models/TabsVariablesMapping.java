package com.kuliza.lending.backoffice.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "bo_config_tabs_variables_mapping", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "tab_id", "variable_id" }) })
public class TabsVariablesMapping extends BaseModel {

	@Column(nullable = false)
	private int tabVariableOrder;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "tab_id")
	private Tabs tab;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "variable_id")
	private Variables variable;

	public TabsVariablesMapping() {
		super();
		this.setIsDeleted(false);
	}

	public TabsVariablesMapping(int tabVariableOrder, Tabs tab, Variables variable) {
		super();
		this.tabVariableOrder = tabVariableOrder;
		this.tab = tab;
		this.variable = variable;
		this.setIsDeleted(false);
	}

	public Tabs getTab() {
		return tab;
	}

	public void setTab(Tabs tab) {
		this.tab = tab;
	}

	public Variables getVariable() {
		return variable;
	}

	public void setVariable(Variables variable) {
		this.variable = variable;
	}

	public int getTabVariableOrder() {
		return tabVariableOrder;
	}

	public void setTabVariableOrder(int tabVariableOrder) {
		this.tabVariableOrder = tabVariableOrder;
	}

}
