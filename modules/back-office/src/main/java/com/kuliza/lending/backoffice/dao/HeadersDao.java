package com.kuliza.lending.backoffice.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.backoffice.models.Cards;
import com.kuliza.lending.backoffice.models.Headers;

@Repository
public interface HeadersDao extends CrudRepository<Headers, Long> {

	public Headers findByHeaderKeyAndIsDeleted(String headerKey, boolean isDeleted);

	public Headers findByHeaderKeyAndCardAndIsDeleted(String headerKey, Cards card, boolean isDeleted);

}
