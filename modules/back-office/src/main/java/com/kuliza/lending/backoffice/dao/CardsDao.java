package com.kuliza.lending.backoffice.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.backoffice.models.Cards;
import com.kuliza.lending.backoffice.models.Tabs;

@Repository
public interface CardsDao extends CrudRepository<Cards, Long> {

	public Cards findById(long id);

	public Cards findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<Cards> findByLabel(String label);

	public List<Cards> findByLabelAndIsDeleted(String label, boolean isDeleted);

	public Cards findByCardsKeyAndIsDeleted(String cardsKey, boolean isDeleted);

	public Cards findByCardsKeyAndTabAndIsDeleted(String cardsKey, Tabs tab, boolean isDeleted);

}
