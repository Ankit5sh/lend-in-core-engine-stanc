package com.kuliza.lending.backoffice.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.backoffice.models.Cards;
import com.kuliza.lending.backoffice.models.CardsVariablesMapping;
import com.kuliza.lending.backoffice.models.Variables;

@Repository
public interface CardsVariablesDao extends CrudRepository<CardsVariablesMapping, Long> {

	public CardsVariablesMapping findByCardAndVariableAndIsDeleted(Cards card, Variables variable, boolean isDeleted);

}
