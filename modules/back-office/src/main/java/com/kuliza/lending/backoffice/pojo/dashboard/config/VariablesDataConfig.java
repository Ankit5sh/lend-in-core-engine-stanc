package com.kuliza.lending.backoffice.pojo.dashboard.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.kuliza.lending.backoffice.models.CardsVariablesMapping;
import com.kuliza.lending.backoffice.models.HeadersVariablesMapping;
import com.kuliza.lending.backoffice.models.TabsVariablesMapping;
import com.kuliza.lending.backoffice.models.Variables;

public class VariablesDataConfig {

	private List<VariablesConfig> variables;

	public VariablesDataConfig() {
		super();
	}

	public VariablesDataConfig(List<VariablesConfig> variables) {
		super();
		this.variables = variables;
	}

	public void TabsVariablesDataConfig(Set<TabsVariablesMapping> tvmappings, Map<String, Object> data) {
		variables = new ArrayList<>();
		for (TabsVariablesMapping tvmap : tvmappings) {
			Variables variable = tvmap.getVariable();
			variables.add(new VariablesConfig(variable, data.get(variable.getMapping())));
		}
	}

	public void CardsVariablesDataConfig(Set<CardsVariablesMapping> cvmappings, Map<String, Object> data) {
		variables = new ArrayList<>();
		for (CardsVariablesMapping cvmap : cvmappings) {
			Variables variable = cvmap.getVariable();
			variables.add(new VariablesConfig(variable, data.get(variable.getMapping())));
		}
	}

	public void HeadersVariablesDataConfig(Set<HeadersVariablesMapping> hvmappings, Map<String, Object> data) {
		variables = new ArrayList<>();
		for (HeadersVariablesMapping hvmap : hvmappings) {
			Variables variable = hvmap.getVariable();
			variables.add(new VariablesConfig(variable, data.get(variable.getMapping())));
		}
	}

	public List<VariablesConfig> getVariables() {
		return variables;
	}

	public void setVariables(List<VariablesConfig> variables) {
		this.variables = variables;
	}

}
