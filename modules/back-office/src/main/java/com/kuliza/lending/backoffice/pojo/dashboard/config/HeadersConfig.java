package com.kuliza.lending.backoffice.pojo.dashboard.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.kuliza.lending.backoffice.models.Headers;
import com.kuliza.lending.backoffice.models.HeadersVariablesMapping;
import com.kuliza.lending.backoffice.models.Variables;
import com.kuliza.lending.backoffice.utils.BackOfficeConstants;

public class HeadersConfig {

	private String label;
	private List<VariablesConfig> variables;

	public HeadersConfig() {
		super();
	}

	public HeadersConfig(String label, List<VariablesConfig> variables) {
		super();
		this.label = label;
		this.variables = variables;
	}

	public HeadersConfig(Headers header, Set<HeadersVariablesMapping> hvmappings, Map<String, Object> data) {
		super();
		this.label = header.getLabel();
		this.variables = new ArrayList<>();
		for (HeadersVariablesMapping hvmap : hvmappings) {
			Variables variable = hvmap.getVariable();
			if (hvmap.getVariable().getType().equalsIgnoreCase(BackOfficeConstants.VAR_TYPE_DROPDOWN)) {
				this.variables.add(
						new VariablesOptionListConfig(hvmap.getVariable(), data.get(hvmap.getVariable().getMapping())));
			} else {
				variables.add(new VariablesConfig(variable, data.get(variable.getMapping())));
			}
		}
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<VariablesConfig> getVariables() {
		return variables;
	}

	public void setVariables(List<VariablesConfig> variables) {
		this.variables = variables;
	}

}
